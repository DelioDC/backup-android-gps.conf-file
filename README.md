# Description

This script makes a backup of /vendor/etc/gps.conf android file to desktop and compress them in .tar.xz format.

# Requirements

 - Bash shell
 - Adb software
 - Permissions to debug the android phone with adb
 
# FAQ

## Requires a rooted device?
No, this script only reads the file who is accesible by all users in read mode.

## Works with LineageOS devices?
Of course! i tested this script in a LineageOS device.