#!/usr/bin/bash
################################################################################
# Author: DelioDC
# Version: 20200219
# Description: Makes a backup of android file /vendor/etc/gps.conf from adb and
#              compress them in tar.xz format.
################################################################################
DATE_TIMESTAMP=$(date +%Y%m%d_%H%M)
DIR_NAME=$DATE_TIMESTAMP"_backup_gps_conf"
PULLED_FILE="./"$DIR_NAME"/gps.conf"
XZ_FILE="./"$DIR_NAME"/"$DATE_TIMESTAMP"_gps.conf.tar.xz"

# make output dir:
mkdir -v $DIR_NAME

# Pull file from android device:
adb devices
adb pull /vendor/etc/gps.conf $PULLED_FILE

# Compress pulled file:
tar --remove-files -cvJf $XZ_FILE $PULLED_FILE $PULLED_FILE
